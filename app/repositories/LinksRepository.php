<?php

namespace App\repositories;

class LinksRepository
{
    protected $data = [
        [
            'id' => 1,
            'user_id' => 4,
            'type' => 'classic',
            'data' => [
                'title' => 'test title 1',
                'url' => 'https:abc.com.au',
                'rest' => 'plus any other fields'
            ]
        ],

        [
            'id' => 2,
            'user_id' => 4,
            'type' => 'classic',
            'data' => [
                'title' => 'test title 2',
                'url' => 'https:abc.com.au2',
                'rest' => 'plus any other fields'
            ]

        ],

        [
            'id' => 3,
            'user_id' => 6,
            'type' => 'music',
            'data' => [
                'title' => 'test title 2',
                'url' => 'https:abc.com.au2',
                'platform_name' => 'spotify',
                'icon' => 'platform thumbnail icon path',
                'audio_code' => 'code snippet copied from platform (spotify etc)',
                'rest' => 'plus any other fields'
            ]

        ],
    ];


    public function search(array $filters): array
    {
        //initial query without filtering
        $dbQuery = $this->data;;

        if (isset($filters['userId'])) {
            //TODO filter by UserId
        }

        if (isset($filters['sort'])) {
            //TODO sort by created_at
        }

        return $dbQuery;
    }

    public function create($type, $data): array
    {
        //TODO validate and insert data into storage & then model/data

        return $data;
    }
}
