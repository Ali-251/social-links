<?php

namespace App\Http\Controllers;

use App\Http\Validations\Links\Exceptions\LinksValidationException;
use App\Http\Validations\Links\LinksValidatorFactoryInterface;
use App\repositories\LinksRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LinksController extends Controller
{
    /**
     * @var LinksValidatorFactoryInterface
     */
    private $factory;
    /**
     * @var LinksRepository
     */
    private $repository;

    public function __construct(LinksValidatorFactoryInterface $factory, LinksRepository $repository)
    {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    //TODO: make sure filters works (allow filtering by userId, sorting etc}

    /**
     * The client must be able to find all links matching a particular userId.
     * The client must be able to find links matching a particular userId, sorted by dateCreated.
     */
    public function index(Request $request): JsonResponse
    {
        return response()->json($this->repository->search($request->all()));
    }

    /**
     * The client must be able to create a new link of each type.
     */
    public function create(Request $request): JsonResponse
    {
        $type = $request->get('type', 'classic');
        $data = $request->input();

        try {
            $validator = $this->factory->create($type, $data);
            $validator->validate();
            $this->factory->create($type, $data);

            return response()->json($this->repository->create($type, $data), 201);

        } catch (\InvalidArgumentException $e) {
            return response()->json($e->getMessage(), 400);

        } catch (LinksValidationException $e) {
            return response()->json($e->getErrors(), 400);
        }
    }
}
