<?php


namespace App\Http\Validations\Links\Exceptions;


use App\Http\Validations\Links\LinksValidatorInterface;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;

class AbstractLinksValidator implements LinksValidatorInterface
{
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var array
     */
    private $data;

    public function __construct(Validator $validator, array $data)
    {
        $this->validator = $validator;
        $this->data = $data;
    }

    /**
     * @throws LinksValidationException
     */
    public function validate(): void
    {
        $data = $this->data;

        $this->validator->validate(
            $data,
            $this->rules(),
            Constraint::CHECK_MODE_APPLY_DEFAULTS
        );

        if (!$this->validator->isValid()) {
            throw new LinksValidationException($this->validator, "invalid payload");
        }
    }

    abstract public function rules(): array;
}
