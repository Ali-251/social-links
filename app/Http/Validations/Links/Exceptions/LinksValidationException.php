<?php

namespace App\Http\Validations\Links\Exceptions;

use JsonSchema\Validator;
use Throwable;

class LinksValidationException extends \Exception
{
    /**
     * @var Validator
     */
    private $validator;

    public function __construct(Validator $validator, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->validator = $validator;
    }

    public function getErrors(): array
    {
        return $this->validator->getErrors();
    }

}
