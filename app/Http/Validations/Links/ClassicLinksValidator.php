<?php

namespace App\Http\Validations\Links;

use App\Http\Validations\Links\Exceptions\AbstractLinksValidator;

class ClassicLinksValidator extends AbstractLinksValidator
{
    public function rules(): array
    {
        return [
            "type' => 'object",
            "properties" => [
                "type" => [
                    "type" => "string",
                    "enum" => ["classic"],
                    'required' => true
                ],
                "title" => [
                    "type" => "string",
                    "maxLength" => 144,
                    'required' => true
                ],
                "url" => [
                    "type" => "string",
                    "format" => "uri",
                    'required' => true
                ],

                "user_id" => [
                    "type" => 'number',
                    'required' => true
                ],
            ]
        ];
    }
}
