<?php

namespace App\Http\Validations\Links;


use App\Http\Validations\Links\Exceptions\LinksValidationException;

interface LinksValidatorInterface
{
    /**
     * @throws LinksValidationException
     */
    public function validate(): void;

    public function rules(): array;
}
