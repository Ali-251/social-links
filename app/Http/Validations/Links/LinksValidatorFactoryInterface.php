<?php

namespace App\Http\Validations\Links;

interface LinksValidatorFactoryInterface
{
    public function create(string $type, array $data): LinksValidatorInterface;
}
