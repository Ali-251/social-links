<?php

namespace App\Http\Validations\Links;

use InvalidArgumentException;
use JsonSchema\Validator;

class LinksValidatorFactory implements LinksValidatorFactoryInterface
{
    const LINKS_TYPE_CLASSIC = 'classic';
    const LINKS_TYPE_MUSIC = 'music';
    const LINKS_TYPE_SHOWS = 'shows';

    public function create(string $type, array $data): LinksValidatorInterface
    {
        switch ($type) {
            case self::LINKS_TYPE_CLASSIC:
                return new ClassicLinksValidator(new Validator(), $data);
            case self::LINKS_TYPE_SHOWS:
                return new ShowsLinkValidator(new Validator(), $data);
            case self::LINKS_TYPE_MUSIC:
                return new MusicLinkValidator(new Validator(), $data);
            default:
                throw new InvalidArgumentException('Invalid link type ' . $type);
        }
    }
}
