# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Schema

```
create table `links` (
id INT UNSIGNED NOT NULL AUTO_INCREMENT,
user_id INT UNSIGNED NOT NULL ,
type VARCHAR(55) NOT NULL,
`data` JSON NOT NULL,
created_at timestamp,
updated_at timestamp,
deleted_at timestamp
);
```

NOTE: This is pseudocode

To allow flexibility of other links and avoid creating many tables, I decided to create one table and store the
relevant fields in `data` column as a JSON

## how to run the
* clone project
* composer install
* run `php -S localhost:8000 -t public`
* Using http client e.g postman
    * post `localhost:8000/links` to create link type (only classic link type is implemented)
    * get `localhost:8000/links` to get all links
    * get `localhost:8000/links?userId=5&sort=dateCreated` to get all links with filter (filtering not implemented)


## extra comments
- used lumen framework I am familiar with Laravel but since its api - Lumen is more lightweight and provided me the familiar structure to Laravel
- I used a json column  to store the custom data for each link type, this would not be an issue using noSQL DB but needs 5.7.8+ for `mysql` DB
- I used external library to help with the validation of different links

    
